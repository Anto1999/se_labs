studenti={}
while True:
    unos=input("Unesi ime, ocijena: ")
    if unos == "":
        break
    ime,ocijena=unos.split(",")
    studenti[ime]=int(ocijena)
    with open("studenti.txt", "w") as f:
        f.write("Studenti:\n")
        for ime in studenti:
            f.write("%s,%d\n"%(ime,studenti[ime]))
            

prosjek=sum(studenti.values())/len(studenti.values())
print("Prosjek je %.2f"%(prosjek))

maxocijena=0
maxstudent=""
for ime in studenti:
    if studenti[ime]>maxocijena:
        maxocijena=studenti[ime]
        maxstudent=ime

print("Student s maximalnom ocijenom je %s" %(maxstudent))

