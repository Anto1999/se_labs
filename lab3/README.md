 Lab 3

## Configure python virtual environment 

https://realpython.com/python-virtual-environments-a-primer/

## Django tutorials

https://docs.djangoproject.com/en/3.1/intro/tutorial01/

For details see: [Official Python tutorial](https://docs.python.org/3/tutorial/index.html)

105  git remote add upstream https://gitlab.com/levara/se_labs
  106  git fetch upstream master
  107  git merge upstream/master
  108  git status
  109  git push
  110  code .
  111  pip
  112  pip install django
  113  git log
  114  ls
  115  cd lab3
  116  ls
  117  pip install virtualenv
  118  ls
  119  mkdir venv
  120  cd venv/
  121  -m venv env
  122  python -m venv env
  123  ls
  124  which python
  125  wich python3
  126  which python3
  127  ls env/
  128  ls env/Scripts/
  129  which python
  130  source env/Scripts/activate
  131  which python
  132  deactive
  133  ls
  134  cd env
  135  ls
  136  cd Scripts/
  137  ls
  138  source activate
  139  pip install django
  140  ls
  141  history
