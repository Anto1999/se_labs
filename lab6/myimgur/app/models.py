from django.db import models
from django.utils import timezone

# Create your models here.

class Image(models.Model):
    title = models.CharField(max_length=128, unique=True, blank=False)
    url = models.CharField(max_length=512)
    description = models.TextField(blank=True)
    pub_date=models.DateTimeField('Published at')
    upvotes = models.IntegerField(default=0)
    downvote = models.IntegerField(default=0)
    
    def __str__(self):
        return self.title

class Comment(models.Model):
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    text = models.TextField(blank=False)
    author = models.CharField(max_length=256, blank=False)


    def __str__(self):
        return f"{self.author}:{self.text[:60]}"