from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from .models import Image, Comment
from django.urls import reverse
# Create your views here.

def index(request):
    images = Image.objects.order_by('-pub_date')
    context = { 'images' : images }
    return render(request, 'app/index.html', context)

def detail(request, image_id):
    image=get_object_or_404(Image, pk=image_id)
    comment_list= image.comment_set.all()
    context = { 'image' : image, 'comments':comment_list }
    return render(request, 'app/detail.html', context)

def comment(request, image_id):
    image= get_object_or_404(Image, pk=image_id)
    try:
        comment = image.comment_set.create(author=request.POST['author'], text = request.POST['comment'])
    except Exception as e:
        return render(request, 'app/detail.html'{'image':image,'error_message':str(e)})
    else:
        return HttpResponseRedirect(reverse('app:detail', args=(image.id,)))